Source: gitpkg
Section: vcs
Priority: optional
Maintainer: Ron Lee <ron@debian.org>
Build-Depends: debhelper (>= 9)
Standards-Version: 4.5.1.0
Vcs-Git: https://salsa.debian.org/ron/gitpkg.git
Vcs-Browser: https://salsa.debian.org/ron/gitpkg

Package: gitpkg
Architecture: all
Depends: git (>= 1:1.7.0), dpkg-dev
Suggests: devscripts
Description: tools for maintaining Debian packages with git
 This package provides tools and automation to assist with maintaining Debian
 packages in git.
 .
  gitpkg        - creates a source package from specified repository versions.
  git-debimport - creates a git repository from a set of existing packages.
 .
 No particular repository layout is required for gitpkg to export source from
 it, existing repositories should require no modification.  If there is a valid
 Debian source tree in there then gitpkg can export a package from any revision
 of it for you.  In the Grand Old Manner these tools are intended to simplify
 specific parts of your existing packaging process and integrate smoothly with
 it, not to replace that with its own singular constraints on how you may work.
 Since not every packaging style suits every package, gitpkg aims to be able to
 work equally well with all of them by sticking to just its part of the job and
 staying out of your way entirely until that job needs to be done.  Hook points
 are provided for performing additional package and user specific operations as
 you may desire or require at export time.

