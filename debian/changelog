gitpkg (0.30) unstable; urgency=medium

  * New utility functions in repo-config-helper which simplify configuration
    option handling in hook scripts.
  * Provide finer grained overrides for usefully configurable options in the
    cowpoke and dpkg-buildpackage exit hooks, so that overriding things like
    the target to build for, or where to upload, don't necessarily override
    all of the other configured defaults.
  * Support creating "source only" changes files for upload, even when doing
    a full binary package build.

 -- Ron Lee <ron@debian.org>  Tue, 10 Aug 2021 01:00:34 +0930

gitpkg (0.29+nmu1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 17:49:28 +0100

gitpkg (0.29) unstable; urgency=medium

  * Fix git-debcherry for a change in the behaviour of how git rev-parse
    handles tag refs.  Thanks to James McCoy for spotting it and the initial
    patch to fix it.  Closes: #887963

  * Support passing arbitrary options to the orig compressor, and default to
    passing -n (--no-name) to gzip, so that it doesn't add a timestamp, making
    the compressed orig tarball more reproducible.  Thanks to Antoine Beaupré
    for noticing the current default behaviour, the idea of passing this option
    to gzip to improve it, and testing that it worked.  The actual fix here is
    a bit different, because -n (meaning this) is specific to gzip, so now we
    have support for setting per-compressor options too.  Closes: #820842

 -- Ron Lee <ron@debian.org>  Sun, 18 Mar 2018 17:29:35 +1030

gitpkg (0.28) unstable; urgency=medium

  * pristine-pre-export-hook: copy with data from pre-1.22 versions of
    pristine-tar that used commits rather than trees.  Closes: #817081.
    Thanks to David Bremner for catching it and patching it.

 -- Ron Lee <ron@debian.org>  Wed, 16 Mar 2016 23:39:42 +1030

gitpkg (0.27) unstable; urgency=medium

  * git-debcherry: Strip trailing slash from patch dir.  Closes: #784130
  * Preserve git notes in git-debcherry.  Closes: #784159
  * Don't include a signature in patch series output.  Closes: #772626

    Thanks to Niko Tyni and Tino Mettler for reports and patches,
    and David Bremner for reviewing the debcherry changes.

 -- Ron Lee <ron@debian.org>  Tue, 11 Aug 2015 23:45:47 +0930

gitpkg (0.26) unstable; urgency=low

  * Avoid ambiguity in the pristine-tar hook when branch and directory names
    are the same, which isn't entirely unlikely if people have a branch named
    'debian'.  Thanks to David Bremner for further testing and a patch.
  * Low urgency because we're in the pre-freeze 'be cautious' slowdown now.

 -- Ron Lee <ron@debian.org>  Thu, 09 Oct 2014 04:44:26 +1030

gitpkg (0.25) unstable; urgency=medium

  * Introduce a new command 'git-debcherry' and an export hook to use it
    'debcherry-deb-export-hook'.  This exports a quilt patch series for
    the differences between 'ref' and 'origref' by analysing the changes
    in the repository to files outside the debian directory.  It allows
    a much more natural workflow of pushing proper patches to git that
    upstream can cherry-pick, and automatically removes them from your
    quilt patch series when they do.  Many, many thanks to David Bremner
    for the many, many hours he spent patiently writing, and testing,
    and rewriting, and refining this until I stopped finding things to
    whine about and saying "wait, but what if we ...".  It's time to see
    if others can find a way to surprise us with it now. Closes: #662726

 -- Ron Lee <ron@debian.org>  Sun, 05 Oct 2014 21:57:17 +1030

gitpkg (0.24) unstable; urgency=medium

  * Actually support orig.tar.xz in git-debimport, by fixing the copy/paste
    fail that returned .bz2 as the extension for those.
  * Update sanitise_git_ref() for the git 2.1.0 rules.
  * Add a command line parser.  Mostly this is for the benefit of hook scripts
    which can reasonably be passed per-invocation options to control their
    behaviour that don't actually change the source package which is exported
    for a particular treeish (such as passing --dist or --arch options to the
    cowpoke exit hook).  For gitpkg itself, the --deb-dir and --exit-hook
    options are similarly 'harmless' so overriding the normal gitconfig can be
    useful to do sometimes.  The cowpoke exit hook can now have its options
    overridden with --cowpoke, and the dpkg-buildpackage exit hook can have
    its options overridden by passing --dpkg-bp.  People who create their own
    hooks can also transparently add their own options for them if needed.
    Closes: #674246
  * Make the --version and --help options work even when not running inside a
    git repository.
  * Report all errors to stderr.  Previously we leaked some of them to stdout.
  * Add a --keep-unpacked-source option (and corresponding gitconfig).
    Previously we always kept the unpacked source directory unless people
    removed it themselves in their own exit hook, but that wasn't everybody's
    ideal behaviour.  Now that it's easy to request this on invocation, let's
    keep the export directory clean by default.

 -- Ron Lee <ron@debian.org>  Mon, 29 Sep 2014 01:51:49 +0930

gitpkg (0.23) unstable; urgency=low

  * Depend on git with an epoch, since the versions of gnuit before the name
    change otherwise satisfy this dep, as do earlier versions of git which
    already have an epoch too.  Thanks to Jonathan Nieder for catching this.
    Closes: #676851

 -- Ron Lee <ron@debian.org>  Sun, 10 Jun 2012 18:42:52 +0930

gitpkg (0.22) unstable; urgency=low

  * Add bash completion for gitpkg.  Closes: #675522
    Many thanks to Stig Sandbeck Mathisen for contributing this.

 -- Ron Lee <ron@debian.org>  Sun, 03 Jun 2012 22:06:43 +0930

gitpkg (0.21) unstable; urgency=low

  * Remove the quilt hook git-patches file if no patches were exported.
    This avoids the lintian check misfiring if people keep a wildcard revision
    range in it but there are no patches to export for a particular release.
    Closes: #633891
  * Don't try to export the --help revision of a package if people pass that
    to gitpkg without reading its manual page first.  This is a community-
    service exception to the usual policy of having no command line options
    (and so no way to screw up an export by passing the wrong ones).
    Closes: #633869

 -- Ron Lee <ron@debian.org>  Sun, 17 Jul 2011 01:52:35 +0930

gitpkg (0.20) unstable; urgency=low

  * Check if PATCHES is empty in quilt-patches-deb-export-hook, to avoid adding
    an empty line to the series file, which will in turn trigger the lintian
    warning about patches not being exported.

 -- Ron Lee <ron@debian.org>  Wed, 13 Jul 2011 22:16:06 +0930

gitpkg (0.19) unstable; urgency=low

  * Move sanitise_git_ref to repo-config-helper.
    Use it in quilt-patches-deb-export-hook to export valid git refnames,
    based on {DEB,UPSTREAM}_VERSION, in {DEB,UPSTREAM}_REF.  Closes: #620969

 -- Ron Lee <ron@debian.org>  Mon, 18 Apr 2011 00:43:25 +0930

gitpkg (0.18) unstable; urgency=low

  * Be nice to emacs users of the quilt-patches hook.
  * We don't call the package clean target by default anymore.  There's a bunch
    of good reasons not to, and not many packages that really need this. If you
    want the old behaviour back, you can enable it with:

    $ git config gitpkg.prebuild-target clean

 -- Ron Lee <ron@debian.org>  Sun, 09 Jan 2011 02:32:59 +1030

gitpkg (0.17) unstable; urgency=low

  * More cool things from David Bremner, quilt-patches-deb-export-hook.
    Closes: #607279

 -- Ron Lee <ron@debian.org>  Sun, 19 Dec 2010 05:56:59 +1030

gitpkg (0.16) unstable; urgency=low

  * Catch the case of two treeishes passed, but a native changelog version.
    Mild hilarity will ensue if we don't, for people who accidentally use a
    native package version when they have debian patches being applied.
    Which is probably as it should be, but we can be friendlier than that.

 -- Ron Lee <ron@debian.org>  Tue, 16 Nov 2010 20:41:43 +1030

gitpkg (0.15) unstable; urgency=low

  * Updates to git-debimport.
  * The git 1.7.0-rc1 release breaks status -a, replacing it with a --dry-run
    option to commit.  There is no option compatible with git from both before
    and after that so go with the flow and version the git Depends accordingly.
    Closes: #587178
  * Switch the git Depends from git-core too while we are at that.
  * Tweak the package sorting a bit further.  Prune off all non-version text
    when feeding the comparator, since the initial find may include packages
    in different subdirs which can otherwise distort the sort.  Protect the
    arithmetic operation in the sort from tripping the `set -e` trap if the
    sort does go all the way up to the first entry in the list.  Thanks to
    Yann Dirson for noticing this and sending patches.  Closes: #587175
  * Add support for format 3.0 packages.  Thanks to Craig Small for a proof
    patch that showed how little actually needed to be changed to provide
    initial support for debian.tar.gz 'diffs'.  Closes: #565503
  * Add support for xz and bz2 compressed packages too.
  * Fix for debsnap gratuitously breaking its argument passing convention,
    but this now works again with snapshot.debian.org if you have debsnap
    from devscripts 2.10.63 or later (modulo some already known bugs in
    debsnap itself with handling certain snapshot data).  Closes: #521764
  * Handle tarballs that don't have their source under a single subdir.
    dpkg-source special cases that, so do something similar here too rather
    than just blindly stripping off the top level when unpacking.
    Closes: #587181

 -- Ron Lee <ron@debian.org>  Sun, 27 Jun 2010 09:51:24 +0930

gitpkg (0.14) unstable; urgency=low

  * Update sanitise_git_ref for changes to git's rules about what is legal.
  * Hook script tweaks:
    - Save a build log in the dpkg-buildpackage exit hook.
    - Ensure the destination directory exists before calling pristine-tar.

 -- Ron Lee <ron@debian.org>  Fri, 01 Jan 2010 07:05:17 +1030

gitpkg (0.13) unstable; urgency=low

  * Lucky for some.
  * Don't hardcode .gitignore removal here, people should use gitattributes(5)
    for that sort of thing instead.
  * Move all configuration options to use git-config(1).  There is no sense in
    reinventing a(nother) config file hierarchy and format ourselves.  Existing
    config should continue working for now, and gitpkg itself will hint to you
    about how to migrate.
  * Pass the contents of gitpkg.dpkg-source options verbatim to dpkg-source.
    Closes: #554519
  * Support being invoked from a subdir of the repo to export.
  * Pander to "format 3.0" which can't build orig tarballs itself anymore and
    gags on the dpkg-source options that specify how the source is presented.
  * Drop the .orig suffix from paths in the orig tarballs.
  * Make the hardcoded rule to call the debian/rules clean target prior to
    building the package more flexible.  Not all packages need this, and some
    may have some other special rule they'd rather call instead.
  * Rehash the user hook handling entirely (though any existing hooks should
    still work for now too).  We now provide support for multiple distinct
    hooks that are run at key times and places during the build process, and
    a more complete set of environment variables are guaranteed to be available
    to them.  Closes: #544210
  * Add /usr/share/gitpkg/hooks initially populated with simple off the shelf
    support for cowpoke, dpkg-buildpackage, and pristine-tar.
  * Now with 30% less typing!  That's right, you only need to pass the second
    treeish if you actually do need gitpkg to export a new orig tarball.
    This isn't really a "new feature", but it's now safe to formalise that it
    isn't just an accident.  The second tag was always ignored previously if
    a suitable orig tarball already existed, but now we give you more choice
    over what should actually happen in that case, and make it explicit that
    you don't need to pass the second treeish if you already have an orig.

 -- Ron Lee <ron@debian.org>  Sun, 15 Nov 2009 06:04:26 +1030

gitpkg (0.12) unstable; urgency=low

  * Ack moving this to the new 'vcs' section of the archive.
  * Don't leak failure from a subshell that didn't really fail.
    Special exceptions can't cross that line.  Closes: #550624.

 -- Ron Lee <ron@debian.org>  Mon, 12 Oct 2009 23:17:28 +1030

gitpkg (0.11) unstable; urgency=low

  * Note in git-debimport(1) that the snapshot.debian.net archive is broken and
    so --fetch cannot currently work with it.
  * Add missing dependencies on git-core and dpkg-dev.  Apparently these things
    aren't already essential :)  Closes: #536221  Thanks!

 -- Ron Lee <ron@debian.org>  Thu, 09 Jul 2009 00:59:25 +0930

gitpkg (0.10) unstable; urgency=low

  * Fix the test for debsnap to not trap on set -e if not present.

 -- Ron Lee <ron@debian.org>  Sat, 10 Jan 2009 23:58:16 +1030

gitpkg (0.9) unstable; urgency=low

  * Add a --fetch option to git-debimport to retrieve packages from
    snapshot.debian.net rather than requiring an existing local archive
    to import.  Many thanks to David Paleino for his work on making that
    a very useful reality.
  * Enable importing Debian 'native' packages too, this is much more useful
    now that we have --fetch.
  * Add a --late-merge option, which implements the previous behaviour wrt to
    merging the upstream branch after an import and default to merging each new
    upstream incrementally now. This gives a much better history if it succeeds
    without conflicts, and lets us still fall back if not.  Closes: #468218
  * Make git-debimport generally more robust.  Being able to kickstart a new
    repo quickly and easily from package snapshots means this might be with us
    and still useful for considerably longer than was initially planned.

 -- Ron Lee <ron@debian.org>  Sat, 10 Jan 2009 09:32:41 +1030

gitpkg (0.8) unstable; urgency=low

  * Reuse an existing orig.tar.gz for single branch exports too.
    Generally it wouldn't be a great idea to package most things with a Debian
    version in this way, but it should Work Right if you do and shouldn't trash
    an existing orig.tar.gz if you use the single tag invocation by mistake.

 -- Ron Lee <ron@debian.org>  Mon, 01 Dec 2008 08:39:45 +1030

gitpkg (0.7) unstable; urgency=low

  * Add support for an absolute path to the packages for git-debimport.
    Thanks to Anuradha Weeraman for the poke to fix this and an initial
    partial patch.  Closes: #482444
  * Add Vcs- headers to control.

 -- Ron Lee <ron@debian.org>  Sun, 08 Jun 2008 03:06:19 +0930

gitpkg (0.6) unstable; urgency=low

  * Strip '.diff.gz' from package names when sorting them

 -- Ron Lee <ron@debian.org>  Wed, 19 Mar 2008 04:51:37 +1030

gitpkg (0.5) unstable; urgency=low

  * Make git-debimport order things the way dpkg would, other people are
    starting to notice that too now.  Sorry if you got burned by that at
    some point -- thank Cyril for poking me to fix it.  Closes: #458298
  * Fix a typo in the reported output of what gitpkg is doing.

 -- Ron Lee <ron@debian.org>  Mon, 31 Dec 2007 02:21:05 +1030

gitpkg (0.4) unstable; urgency=low

  * Filter out epochs from the DEB_VERSION.  Thanks to phil and dato.
  * Add a section on workflow to gitpkg.1 as one simple example of a way things
    can be done.  Tips, improvements, and alternative to that are welcome.

 -- Ron Lee <ron@debian.org>  Wed, 26 Dec 2007 00:25:23 +1030

gitpkg (0.3) unstable; urgency=low

  * Sanitise git-debimport tag names extracted from package version strings.
    Some things that are valid version strings are not valid tag refnames.

 -- Ron Lee <ron@debian.org>  Thu, 29 Nov 2007 21:15:57 +1030

gitpkg (0.2) unstable; urgency=low

  * Now with extra configurable ability *

  * Read some configurable things from external config files.
  * When exporting source, place each project under its own unversioned tree
    in the output directory to avoid clutter when multiple projects share the
    same output location.
  * Execute a per-project hook script if it exists in the checked out source,
    just before dpkg-source is called.

  * Include git-debimport patch from Romain Francoise to sanitise the top level
    directory name from the orig.tar.gz and extract author and date information
    from the orig.tar.gz and debian/changelog.  Thanks!

 -- Ron Lee <ron@debian.org>  Mon, 26 Nov 2007 04:03:38 +1030

gitpkg (0.1) unstable; urgency=low

  * Initial Release.  Closes: #443392

 -- Ron Lee <ron@debian.org>  Fri, 21 Sep 2007 10:32:55 +0930
